;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Use Emacs packages
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'package)

;; Use Melpa package archives
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)

(package-initialize)

;; Get list of packages
(when (not package-archive-contents)
  (package-refresh-contents))

;; Fix to bad signature problem
;; 1. (set-variable 'package-check-signature nil)
;; 2. (package-refresh-contents)
;; 3. (package-install 'gnu-elpa-keyring-update)
;; 4. restart Emacs

;; It's possible to get some packages from Ubuntu repositories
;;
;; apt install elpa-use-package elpa-go-mode elpa-magit elpa-markdown-mode

;; Install and enable use-package if not present
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Basic UI setup
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(tool-bar-mode -1)  ;; Disable toolbar 
(show-paren-mode 1) ;; Show matching parentheses
(setq-default indent-tabs-mode nil)  ;; Don't mix tabs into indents

;; Fix various annoyances or bad default behaviour
(setq save-interprogram-paste-before-kill t
      mouse-yank-at-point t
      require-final-newline t
      visible-bell t
      load-prefer-newer t
      backup-by-copying t
      column-number-mode t
      inhibit-startup-screen t)

;; Tab will first indent, then complete
(setq tab-always-indent 'complete)

;; Enable recent files
(recentf-mode 1)

;; Save backups in ~/.emacs.d/backups instead of littering ~ files around
(setq backup-directory-alist `(("." . ,(concat user-emacs-directory
                                               "backups"))))
(server-start)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Theme
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Newest version from MELPA doesn't support Emacs < 28.1
;;
;; cd Downloads
;; wget https://github.com/protesilaos/modus-themes/archive/refs/tags/4.6.0.zip
;; unzip 4.6.0.zip
;; (package-install-file "~/Downloads/modus-themes-4.6.0")

(use-package modus-themes :ensure t
  :config
  (load-theme 'modus-operandi-deuteranopia t)  ;; light
  ;; (load-theme 'modus-vivendi-deuteranopia t)  ;; dark
  )


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Packages and package-settings
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Mode for go language
(use-package go-mode :ensure t
  :hook (before-save . gofmt-before-save)
  :config
  (if (file-exists-p "~/go/bin/goimports")
      (setq gofmt-command "~/go/bin/goimports")))

;; apt install python3-flake8 python3-mypy python3-pylint-common
;; pylint --disable=invalid-name --generate-rcfile > .pylintrc

;; Use flymake with python
(use-package flymake :ensure t
  :hook (python-mode . flymake-mode)
  :config
  (setq python-flymake-command '("flake8" "-")))

;; Use eglot for programming
;; - Python needs pylsp (apt install python3-pylsp)
;; - Go needs needs gopls (apt install gopls)
(use-package eglot :ensure t)

;; Company mode for nice tab-completion
(use-package company :ensure t
  :config
  (global-company-mode))

;; Git user interface - https://magit.vc/
(use-package magit :ensure t
  :config
  (add-to-list 'exec-path "/opt/rh/rh-git218/root/usr/bin/"))

;; Markdown
(use-package markdown-mode :ensure t)

;; Unfill, reverse of fill-paragraph M-q
(use-package unfill :ensure t
  :bind (("M-Q" . unfill-paragraph)))

;; Use sh-mode for Singularity def files
(add-to-list 'auto-mode-alist '("\\.def$" . sh-mode))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Key shortcuts
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Compile and jump between errors
(global-set-key (kbd "<f8>") 'compile)
(global-set-key (kbd "<M-down>") 'next-error)
(global-set-key (kbd "<M-up>") 'previous-error)

;; Handy shortcut to open corresponding header/source file in C/C++
(add-hook 'c-mode-common-hook
  (lambda() 
    (local-set-key  (kbd "C-c o") 'ff-find-other-file)))

;; Access recent files
(global-set-key (kbd "C-c r") 'recentf-open-files)

;; Use Ctrl-c s to access scratch.txt
(global-set-key (kbd "C-c s")
                (lambda () (interactive) (find-file "~/Documents/scratch.txt")))

;; Moving between windows
(global-set-key (kbd "C-x <left>")  'windmove-left)
(global-set-key (kbd "C-x <right>") 'windmove-right)
(global-set-key (kbd "C-x <up>")    'windmove-up)
(global-set-key (kbd "C-x <down>")  'windmove-down)

;; Put custom stuff in separate file
(defconst custom-file (expand-file-name "custom.el" user-emacs-directory))
(load custom-file t)
